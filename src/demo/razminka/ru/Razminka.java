package demo.razminka.ru;
import java.util.Arrays;
public class Razminka {
    public static void main(String[] args) {
        int[] one = {1, 1, 1, 1, 1};
        int result = 3;
        Arrays.sort(one);
        System.out.println(Arrays.toString(one));

        int[] expensive = countOf(one, result);

        int summa = 0;
        for (int i = 0; i < expensive.length; i++) {
            summa += expensive[i];
        }
        System.out.println("Cтоимость  заказов: " + summa);
    }

    private static int[] countOf(int[] array, int res) {
        int[] expensive = new int[res];
        if (res > array.length) {
            res = array.length;
        }
        for (int i = 0; i < res; i++) {
            expensive[i] = array[array.length - i - 1];
        }
        return expensive;
    }
}

